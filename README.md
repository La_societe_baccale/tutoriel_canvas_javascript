# Tutoriel_canvas_javascript

Le but de ce tutoriel est d'apprendre à manier l'élement html `<canvas>` pour 
créer une grille de jeu de morpions.Ce cours utilisera 

**1.  Affichage de l'élément canvas**

`<canvas>` est un nouvel élément HTML qui peut être utilisé pour dessiner des 
éléments graphiques à l'aide de scripts (habituellement JavaScript).

Un élément canvas basique délimite une zone sur la page dans laquelle nous 
allons pouvoir dessiner pour créer notre jeu de morpions.

```html
//exemple classique de canvas
<canvas width="300" height="300"></canvas>
```

L'élément canvas est défini par deux attributs:
- la largeur: *width*
- la hauteur: *height* 

Ces deux élément définissent la taille que va prendre l'élément canvas sur 
l'écran. L'unité utilisé est le pixel. Dans notre précédent exemple le canvas 
aura donc une longueur de 300 pixels et une hauteur de 300 pixels.

Essayons d'afficher notre élément canvas dans un navigateur. Nous allons créer
un dossier `tutoriel morpion` dans lequel nous allons créer un fichier 
`index.html` qui contiendra le code HTML de notre jeu de morpions.


```html
//index.html
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Jeu de morpions</title>
        <style>
        body {
            background-color: black;
            color: white;
            text-align: center;
        }
        #morpions {
            background-color: white;
            margin-left: auto;
            margin-right: auto;
            display: block;
        }
        </style>
    </head>
    <body>
        <p id="message">Bienvenue sur le jeu morpions!</p>
        <canvas id="morpions" width="300" height="300"></canvas>
    </body>
</html>
```
On ajoute un id à notre canvas pour pouvoir manier l'élément avec plus de
facilité dans notre code javascript par la suite. On ajoute aussi un petit 
message au dessus du canvas.
A l'intérieur de la balise `<head>` on ajoute du css dans une balise `<style>`
On commence par rendre la page noire avec 'background-color: black' puis on rend
toutes les écriture blanche avec 'color: white'
Pour centrer notre élément canvas on va le traiter comme un block 
('display: block'), en utilisant des marges automatique. On rend aussi l'élément
canvas blanc pour qu'on puisse le voir à l'écran.


En ouvrant le fichier `index.html` depuis notre navigateur voici ce que nous 
devrions voir.
![Premier affichage de notre élément canvas](img/canvas_premier_exemple.png)

**2.  Affichage de la grille sur notre élément canvas**

Pour afficher notre grille dans notre élément canvas nous allons utiliser du 
javascript. C'est à l'aide du code javascript que nous allons "dessiner" dans 
le canvas.

On va donc créer un fichier morpions.js

```javascript
//morpions.js
let canvas = document.getElementById('morpions');
let contexteCanvas = canvas.getContext('2d');
```
 Nous utilisons `let`pour déclarer une variable. C'est globalement le même 
 comportement que `var`. 
 
>*remarque:*
>Nous utiliserons dans ce cours la convention "Camel case". C'est à dire que nous 
>n'utilisons pas d'espaces et que le début des mots (excepté le premier) sont en 
>majuscule. Dans notre cas pour dire "contexte du canvas" nous ecrivons 
>"contexteCanvas".

Reprenons notre code javascript, la première ligne stocke dans la variable "canvas"
l'élément du DOM canvas. On utilise getElementById pour chercher l'élément avec 
l'id "morpions". C'est à dire notre élément canvas dans notre HTML.

Dans la deuxième ligne on utilise la *méthode* `getContext()` qui va créer 
l'objet qui représente la surface du canvas sur laquelle nous allons pouvoir 
dessiner. On précise à la méthode que nous allons travailler en 2d en lui 
précisant en *argument* `2d`.

Nous allons commencer par tracer les lignes permettant de dessiner la grille. 
Pour cela nous allons utiliser plusieurs méthodes à la suite: 
- la méthode `beginpath()` sur notre objet `contexteCanvas`.Cette méthode permet de dessiner une nouvelle ligne. On l'utilisera à chaque fois que l'on veut 
tracer une nouvelle ligne. 
- On utilise ensuite la méthode `moveTo()` pour initier la ligne en lui disant où elle doit commencer. 
- La méthode `lineTo()` pour lui dire ou elle doit finir
- la méthode `stroke()` pour lui dire de s'arreter

Pour tester cela dessinons une première ligne grace à la fonction dessinerGrille()
que nous allons créer dans notre fichier morpions.js:

```js
let canvas = document.getElementById('morpions');
let contexteCanvas = canvas.getContext('2d');

dessinerGrille();

function dessinerGrille () {
    contexteCanvas.beginPath();
    contexteCanvas.moveTo(100, 0 );
    contexteCanvas.lineTo(100, 300);
    contexteCanvas.stroke();

```
 Une fois cette première ligne tracée regardons les arguments que nous avons donnés aux méthodes
 `moveTo()` et `lineTo()`. On donne en premier argument l'abscisse "x" et en second argument l'ordonnée
 "y".
 
 ![Coordonnées canvas](img/coordonées_Canvas.png)

On va donc tracer trois autres lignes pour faire notre grille. Nous voulons que notre grille
puisse s'adapter si l'on change la largeur ou la longueur du canvas (Nous avons
pour l'instant défini à 300 pixels les deux dans notre fichier html). Pour cela définissons 
deux nouvelles variables `hauteurCellule` et `largeurCellule`. Nous voulons que ces deux variables
soient respectivement égales au tier de la hauteur et de la largeur de notre élément canvas.
La largeur et la longeur de l'élement canvas sont donnée par deux propriétés: `canvas.width`et `canvas.height`.
Ensuite nous allons utiliser nos deux nouvelles variables dans les argument des méthodes 
`moveTo()`et `lineTo()`

Nous allons également décider de la couleur de nos lignes en utilisant la 
propriété `strokeStyle` et en y mettant la valeur "black".  Nous allons aussi
élargir nos lignes avec la propriété `lineWidth`. Nous allons y mettre la valeur 
"10" pour faire des lignes de 10 pixels de large.

```js
let canvas = document.getElementById('morpions');
let contexteCanvas = canvas.getContext('2d');

let largeurCellule = canvas.width/3;
let hauteurCellule = canvas.height/3;

dessinerGrille();

function dessinerGrille () {
    contexteCanvas.strokeStyle = 'black';
    contexteCanvas.lineWidth = 10;



    contexteCanvas.beginPath();
    contexteCanvas.moveTo(largeurCellule, 0 );
    contexteCanvas.lineTo(largeurCellule, 3*hauteurCellule);
    contexteCanvas.stroke();

    contexteCanvas.beginPath();
    contexteCanvas.moveTo(2*largeurCellule, 0 );
    contexteCanvas.lineTo(2*largeurCellule, 3*hauteurCellule);
    contexteCanvas.stroke();

    contexteCanvas.beginPath();
    contexteCanvas.moveTo(0, hauteurCellule );
    contexteCanvas.lineTo(3*largeurCellule, hauteurCellule);
    contexteCanvas.stroke();

    contexteCanvas.beginPath();
    contexteCanvas.moveTo(0, 2*hauteurCellule );
    contexteCanvas.lineTo( 3*largeurCellule, 2*hauteurCellule);
    contexteCanvas.stroke();
}
```

**3.  Gestion des évènements**

Nous allons rajouter trois évènements dans notre fichier javascript. En effet 
lorsque le joueur clique il faudra lancer des fonctions pour dessiner la croix 
ou le rond, voir si le joueur a gagné,... 
Pour savoir sur quelle case le joueur clique nous ajoutons trois évènements. Ils 
vont permettre de savoir quand est-ce que le joueur clique et où est la 
souris. 
Nous aurons besoin de stocker l'abscisse et l'ordonnée de la position de 
la souris sur notre canvas. Nous utiliserons donc la variable `souris`dans laquelle
nous allons stocker un objet avec deux propriétés `x`et `y`. 


```js
souris = {
    x: -1,
    y: -1,
};
```

Pour ajouter un évènement nous utilisons la méthode `addEventListener()` sur 
notre élèment canvas. Nous spécifions dans le premier argument de la méthode de 
quel type d'argument il s'agit. Ici nous utilisons:
- `mouseout` qui est déclenché lorsque la souris est déplacée hors de l'élèment
canvas. Lorsqu'il sera déclenché nous stockerons dans les deux propriétés de 
l'objet souris (c'est à dire l'abscisse 'x' et l'ordonnée 'y') la valeur '-1'. 
Comme cela quand le joueur cliquera et que l'objet souris ressemblera à
`{x: -1, y:-1}` nous saurons que le joueur a cliqué en dehors de la grille et qu'il
n'y a donc rien à faire.
- `mousemove`  qui est déclenché quand la souris est déplacé dans l'élément canvas. On stockera la aussi
les coordonnées dans l'objet souris. Pour calculer les coordonnées de la position de la souris 
dans le canvas nous utilisons les propriétés pageX et pageY qui nous donne les 
coordonnées de la souris sur la page. Pour l'abscisse nous retirons la valeur 
de la propriété offsetLeft du canvas qui donne le nombre de pixels à gauche du canvas. 
Pour l'ordonnée nous retirons propriété offsetTop du canvas qui donne le nombre de pixels en haut du canvas.  
-`click` qui sera déclenché lorsque l'utilisateur cliquera. Nous allons créer dans les étapes 
qui suivent un fonction permettant de lancer un tour de jeu.

```js
canvas.addEventListener('mouseout', function () {
    souris.x = souris.y = -1;
});

canvas.addEventListener('mousemove', function (e) {
    let x = e.pageX - canvas.offsetLeft,
        y = e.pageY - canvas.offsetTop;

        souris.x = x;
        souris.y = y;
});

canvas.addEventListener('click', function (e) {
    //ici nous ajouterons la fonction que permettra de lancer un tour de jeu
});
```
**4.  Dessin de la croix et du rond lorsque le joueur clique**

Nous allons maintenant commencer la fonction jouerUnTour() qui va nous permettre de lancer un tour
de jeu lorsque le joueur clique.
Nous allons lui donner en arguments la position de la souris lorsqu'il clique. Nous appelons donc la fonction lors de l'évènement 
"click".


```js
canvas.addEventListener('click', function (e) {
    jouerUnTour(souris);
});
```

Regardons de plus près la fonction jouerUnTour():
```js
function jouerUnTour(souris) {
    let centreCellule = new Object();
    centreCellule.x = Math.floor(souris.x / largeurCellule)*largeurCellule+1/2*largeurCellule;
    centreCellule.y = Math.floor(souris.y / hauteurCellule)*hauteurCellule+1/2*hauteurCellule;
    
    if (aQuiDeJouer === false){
        dessinerRond(centreCellule);
    }
    else{
        dessinerCroix(centreCellule);
    }
    aQuiDeJouer = !aQuiDeJouer;
}
```
La première chose faite est de trouver le centre de la cellule sur lequel a cliquer 
l'utilisateur. En effet pour dessiner notre cercle ou notre rond, il faut le dessiner au milieu 
de la cellule et non pas la où le joueur a cliqué. Pour le faire, on va créer un objet centreCellule qui va stocker les coordonnées 
du centre de la cellule. Pour la cellule en haut à gauche (la cellule 0), le centre est 
(50,50). Les Formules pour trouver le x et le y ne sont pas très importantes ici, retenez 
juste que l'objet centreCellule nous permet de savoir où dessiner notre cercle ou
notre rond.

Lors de l'appel jouer un tour, le script doit générer une croix ou un rond en 
fonction de qui joue. Pour définir qui joue on va utiliser une variable binaire
qui va changer de valeur à chaque fois que l'on fait appel à jouerUnTour
On va donc déclarer au début de notre fichier la variable aQuiDeJouer. Lorsqu'elle 
sera à false, ce sera le joueur rond qui jouera. A l'inverse lorsque aQuiDejouer 
sera égale à true c'est à croix de jouer. Nous décidons arbitrairement que les ronds vont 
commencer en initialisant la variable à false.

```js
let aQuiDeJouer = false;
```
A la fin de la fonction jouerUnTour() on change la valeur de aQuiDeJouer en utilisant le point d'exclamation
devant la variable.

Dans la fonction jouerUnTour(), pour savoir si l'on dessine une croix ou un rond on va utiliser 
`if`. Cela nous permet de dessiner un rond avec la fonction dessinerUnRond() lorsque aQuiDeJouer
est à false. Et de dessiner une croix avec la fonction dessinerUneCroix() lorsque aQuiDeJouer
est à true.


La fonction dessinerUneCroix() utilise les mêmes méthodes que pour dessiner la grille
(beginPath, moveTo, lineTo, stroke) en dessinant deux lignes qui vont créer la croix 
à partir des coordonnées du centre de la cellule.

```js

function dessinerCroix (centreCellule) {
    
    contexteCanvas.beginPath();
    contexteCanvas.moveTo(centreCellule.x-largeurCellule/2, centreCellule.y-hauteurCellule/2);
    contexteCanvas.lineTo(centreCellule.x+largeurCellule/2, centreCellule.y+hauteurCellule/2);
    contexteCanvas.stroke();
    
    contexteCanvas.beginPath();
    contexteCanvas.moveTo(centreCellule.x+largeurCellule/2, centreCellule.y-hauteurCellule/2);
    contexteCanvas.lineTo(centreCellule.x-largeurCellule/2, centreCellule.y+hauteurCellule/2);
    contexteCanvas.stroke();
}
```

Pour la fonction dessinerUnRond() on utilise la méthode `arc()`. Qui fonctionne un peu de la 
même manière que pour les lignes avec les méthodes `beginPath()`et `stroke()`.
Elle prend 5 arguments en comptes
- l'abscisse du centre de l'arc : dans notre cas il s'agit de centrecellule.x
- l'ordonnée du centre de l'arc : dans notre cas il s'agit de centrecellule.y
- le rayon de l'arc : pour avoir un cercle qui rentre correctement dans notre damier
nous allons utiliser la valeur hauteurCellule divisé par 2,5
- l'angle de début de l'arc : arc utilise le système de mesure en radian pour les 
angles. Du coup pour dessiner un cercle complet on doit partir de 0 pour aller 
jusqu'à 2π. Si on va jusqu'à π seulement on va dessiner un arc de cercle. On rentre
donc 0 pour cet argument.
- l'angle de la fin de l'arc : du coup on utilise Math.PI*2

```js
function dessinerRond (centreCellule) {
    contexteCanvas.beginPath();
    contexteCanvas.arc(centreCellule.x, centreCellule.y, hauteurCellule / 2.5, 0, Math.PI*2 );
    contexteCanvas.stroke();
}
```

Et voilà un coup sur deux on dessine bien un rond ou une croix seulement personne ne gagne jamais et on va devoir


**5. Qui gagne? **

Pour savoir qui gagne nous devons faire en sorte que le script "se rappelle" de là où ont cliqué les
deux joueurs au cours de la partie. Nous allons donc attribuer un numéro à chacune des cases. Ensuite pour chacune 

Nous allons donc créer la fonction caseCanvasParCoordonnees qui utilisera deux arguments
x et y et qui nous renverra un nombre entre 0 et 8. Chacune de nos case est numérotée de 0 à 8
comme le montre l'image suivante

![Premier affichage de notre élément canvas](img/numérotation_des_cellules.png)

Maintenons que nous avons attribué un numéro à chaque cellule créons la fonction
qui retourne le numéro de la cellule sur laquelle l'utilisateur a cliqué.

```js
function caseCanvasParCoordonnees (x, y) {
    return (Math.floor(x / largeurCellule) % 3) + Math.floor(y / hauteurCellule) * 3;
}
```

Pour comprendre comment marche cette fonction prenons un exemple:
>Si la souris a pour coordonées {x=50,Y=50} alors la souris est au milieu de la cellule 0.
>Si nous divisons par la largeur de la cellule ici 100 nous obtenons 0,5. La fonction math.floor() 
>nous donne l'entier le plus proche en dessous de la valeur qu'on lui donne. Ici pour 0,5 elle nous 
>donnera 0. Pour deuxième partie du calcul, nous divisons notre y (ici 50) par la 
>hauteur de la cellule (ici 100) ce qui donne 0,5. Avec la fonction math.floor() nous 
>retrouvons 0. Le calcul donne donc "0 + 0 x 3" ce qui donne 0. Cela nous donne donc le bon numéro de cellule.
>Vous pouvez essayer avec d'autres coordonnées pour mieux comprendre le fonctionnement de cette fonction mais 
>ce n'est pas fondamental ici.




Sources:
- https://developer.mozilla.org/fr/docs/Tutoriel_canvas
- https://github.com/GeekLaunch